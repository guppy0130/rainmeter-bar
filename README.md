# Rainmeter Bar

A minimal bar across the top of your screen.

Written by Guppy0130/Nick Yang. Licensed under GNU GPLv3.

## Changelog

01/18/20: Use a single font, no FontAwesome. Please patch fonts with NerdFont patcher.  
07/31/18: Fixes for Spotify dropping LocalAPI. Requires Spicetify. Dropped bundled dlls.  
02/16/18: First Release. Comes with SpotifyPlugin v1.9.3.0, written for Win10 + Rainmeter 4.2.0r3029  

## Screenshots
### Ethernet
![Ethernet-no-music](screenshots/ethernet-no-music.png)
![Ethernet-playing](screenshots/ethernet-playing.png)
![Ethernet-paused](screenshots/ethernet-paused.png)
![Ethernet-muted](screenshots/ethernet-muted.png)
### Wireless
![Wireless-no-music](screenshots/wireless-no-music.png)

## Installation
### Prerequisites
Install Rainmeter v4.2.0r3111.  
Install [Spicetify](https://github.com/khanhas/Spicetify/releases) (tested with v1.7.4).  
Backup your Spotify installation with Spicetify, then enable the WebNowPlaying companion in the "extensions" section of Spicetify (may be on the second page). Apply changes.  
Install the font [Envy Code R](https://damieng.com/blog/2008/05/26/envy-code-r-preview-7-coding-font-released).

### Skin Installation
Use the `.rmskin` package (which will load the theme automatically), or install with git:

```powershell
cd $env:userprofile/Documents/Rainmeter/skins
# or where your Rainmeter installation is
git clone https://github.com/guppy0130/rainmeter-bar.git
# or git clone https://gitlab.com/guppy0130/rainmeter-bar.git
```

Open Rainmeter, open folder `rainmeter-bar`, and double-click `Bar.ini` to load it.

## Modifying
Modify `Bar.ini` (fonts, measures, plugins, etc).  
Please refer to `LICENSE` for proper citations. A `LICENSE` file should be available in all derivatives of this work, as well as the original work itself.
